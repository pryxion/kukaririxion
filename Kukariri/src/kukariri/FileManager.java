package kukariri;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;

public class FileManager {
    
    public static final Path folderPath = Paths.get(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "\\Pryxion\\Kukaririxion\\");
    public static final Path filePath = Paths.get(folderPath.toString() + "\\save.txt");
    
    public boolean getFileExistence() {
        return filePath != null;
    }
    
    public String load() throws IOException {
        ArrayList<String> lines = new ArrayList<String>();
        
        lines = (ArrayList<String>) Files.readAllLines(filePath);
        
        return lines.get(0);
    }
    
    public void save(int level) throws IOException {
        new File(folderPath.toString()).mkdirs();
        
        ArrayList<String> lines = new ArrayList<String>();
        
        File f = new File(filePath.toString());
        
        if (f.exists()) {
            lines = (ArrayList<String>) Files.readAllLines(filePath);
            lines.remove(0);
        }
        
        lines.add(level + "");
        
        Files.write(filePath, lines, Charset.forName("UTF-8"));
    }
}
