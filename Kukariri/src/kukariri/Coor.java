package kukariri;

public class Coor {
    public int x, y;
    
    public Coor(int x, int y) {
        
        this.x = x;
        this.y = y;
    }
    
    public int getX() {
        
        return x;
    }
    
    public int getY() {
        
        return y;
    }
    
    public boolean isEqualTo(Coor coor) {
        if (x == coor.x && y == coor.y) {
            return true;
        } else {
            return false;
        }
    }
}
