
package kukariri;

// Start Timer:

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;

// setRunning(true);

// Update Timer:
// if (getRunning())
//     increaseCounter();

// Complete-Do:
// if (getIsComplete(false)) {
//     //...
// }

// Complete-Do-Reset:
// if (getIsComplete(true)) {
//     //...
// }

public class Timer implements ActionListener {
    
    private int delay;
    private int counter;
    private boolean isRunning;
    private boolean isComplete;
    
    public Timer(int delay) {
        this.delay = delay;
        
        initTimer ();
    }
    
    void initTimer () {
        javax.swing.Timer timer = new javax.swing.Timer(1, this);
        timer.start();
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
    
    public boolean getRunning() {
        return isRunning;
    }
    
    public void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }
    
    public boolean getComplete() {
        return isComplete;
    }
    
    public void setComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public void resetCounter() {
        counter = 0;
    }

    public void increaseCounter() {
        counter++;
    }
    
    public void resetTimer() {
        setComplete(false);
        resetCounter();
    }
    
    void updateTimer() {
        if (getRunning()) {
            increaseCounter();

            if (counter >= delay) {
                setComplete(true);
                resetCounter();
                setRunning(false);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        updateTimer();
    }
}
